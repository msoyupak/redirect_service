configure do
  db = YAML::load(File.open('./config/database.yml.local'))['development']  
  ActiveRecord::Base.establish_connection(
    :adapter  => db['adapter'],
    :host     => db['host'],
    :username => db['username'],
    :password => db['password'],
    :database => db['database']
  )
end
 
