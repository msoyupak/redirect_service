ActiveRecord::Migration.create_table :mappings do |t|
  t.string :from
  t.text :to
  t.integer :count
  t.text :log
  t.timestamps
end
