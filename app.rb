require 'sinatra'
require_relative 'mapping'
require_relative './config/config.rb'

class App < Sinatra::Application
 
end

get '/urls' do
  Mapping.all.to_json
end

get '/urls/:id' do
  m = Mapping.find_by_from(params[:id])
  if m.nil?
    halt 404
  else
    m.to_json
  end
end
 
delete '/urls/:id' do
  m = Mapping.find_by_from(params[:id])
  if m.nil?
    halt 404
  else
    m.destroy
    {:success => "ok"}.to_json
  end
end

post '/urls' do
  m = Mapping.new(params)
  if m.save
    m.to_json
  else
    halt 400
  end
end 

put '/urls/:id' do
  m = Mapping.find_by_from(params[:id])
  if m.nil?
    halt 404
  else
    m.update(params.reject{|k,v| !m.attributes.keys.member?(k.to_s)})
    m.save
    m.to_json
  end
end

get '/*' do
  redir = params[:splat]
  m = Mapping.find_by_from(redir)
  redirect m.to unless m.nil?
end
